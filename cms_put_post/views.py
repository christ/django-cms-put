from django.shortcuts import render

# Create your views here.

from django.http import HttpResponse
from .models import Contenido
from django.views.decorators.csrf import csrf_exempt


form = """
<form action="" method="POST">
    <p>Introduce el valor: <input type="text" name="valor"/>
    <p><input type="submit" value="Enviar"/>
</form>
"""

@csrf_exempt
def get_content(request, llave):
    if request.method == "PUT":
        valor = request.body.decode('utf-8')
        try:
            c = Contenido.objects.get(clave=llave)
            c.valor = valor
        except Contenido.DoesNotExist:
            c = Contenido(clave=llave, valor=valor)
        c.save()

    if request.method == "POST":
        valor = request.POST['valor']
        try:
            c = Contenido.objects.get(clave=llave)
            c.valor = valor
        except Contenido.DoesNotExist:
            c = Contenido(clave=llave, valor=valor)
        c.save()
    try:
        contenido = Contenido.objects.get(clave=llave)
        respuesta = "El valor de la clave " + contenido.clave \
                    + " es " + contenido.valor + ", y su id es " + str(contenido.id)
    except Contenido.DoesNotExist:
        respuesta = "No existe contenido para " + llave + form
    return HttpResponse(respuesta)

